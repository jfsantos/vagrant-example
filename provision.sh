# provision.sh: update apt database and install GNU Octave
export DEBIAN_FRONTEND=noninteractive
apt-get update > /dev/null
apt-get -y install octave
